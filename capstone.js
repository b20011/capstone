class Product {
  constructor(name, price, isActive) {
    this.name = name;
    this.price = price;
    this.isActive = true;
  }

  archive() {
    this.isActive ? false : true;
    return this;
  }

  updatePrice(newPrice) {
    this.price = newPrice;
    return this;
  }
}

console.log('=============Products=============');
const toast = new Product('Toast', 1.55);
console.log(toast.updatePrice(2.55));

class Cart {
  constructor() {
    this.contents = [];
    this.totalAmout = 0;
  }

  addToCart(product, quantity) {
    this.contents.push(new Product(product, quantity));
    return this;
  }

  showCartContents() {
    return this.contents;
  }

  updateProductQuantity(name, quantity) {
    const filtered = this.contents.filter((el) => {
      if (el.name === name) {
        el.quantity = quantity;
      }
    });

    if (filtered) {
      return this;
    }
  }

  clearCartContents() {
    this.contents = [];
  }

  computeTotal() {
    this.contents.forEach((el) => {
      //     if (el.quantity > 1) {
      //       el.
      //   }
      console.log(el);
    });
  }
}

console.log('=============CART=============');
const toastCart = new Cart();
toastCart.addToCart(toast, 2);
console.log(toastCart.showCartContents());

class Customer {
  constructor(email) {
    this.email = email;
    //   cart - instance of Cart class
    this.orders = [];
  }

  checkOut() {
    if (this.orders.length === 0) {
      //cart to be pushed
    }
  }
}
